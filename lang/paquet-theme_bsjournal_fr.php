<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// T
	'theme_bsjournal_description' => 'Crisp like a new sheet of paper',
	'theme_bsjournal_slogan' => 'Crisp like a new sheet of paper',
);
